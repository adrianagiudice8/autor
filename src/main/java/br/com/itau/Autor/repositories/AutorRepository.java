
package br.com.itau.Autor.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.Autor.models.Autor;

public interface AutorRepository extends CrudRepository<Autor, Integer>{

}

